﻿using UnityEngine;
//using System;
using System.Collections;
using System.Collections.Generic;

public class worldCell : MonoBehaviour, System.IEquatable<worldCell>, System.IComparable<worldCell>{

	//working//public worldCell[] neighbors = new worldCell[4];
    public worldCell[] neighbors = new worldCell[6];//0 = forward, 1 = left, 2 = back, 3 = right, 4 = up, 5 = down
	List<int> neighborTypes = new List<int>();
	public Vector3 location;

	//public int oldTile;
	public int newTile;
	private bool forced;//allows the creater to force a land type if true;

	public bool visited = false;
	public int entryCost =  int.MaxValue;
    public int costPathToHere = int.MaxValue;//BIG number

    bool autoExpand;
    int tileType;

    public bool skipStart;

    // Use this for initialization
	void Start () {
        if (skipStart)//this is here so that I can use pre made maps without the whole thing trying to rebuild itself.
        {
            return;
        }
        Transform cube = null;
        if (!forced)
        {// use this to pick the next land type as long as it has not been manualy selected already.
            newTile = 0;//make sure that newTile does not stay -1;
            foreach (Vector3 dir in map.directions)
            {
                Vector3 modifiedLocation = (location + dir);
                GameObject go = map.get(modifiedLocation);
                if (go != null)
                {
                    worldCell script = go.GetComponent<worldCell>();
                    neighborTypes.Add(script.newTile);
                }
            }
            //decide the winner of the surrounding land types that have already been selected
            int winner = Random.Range(0, neighborTypes.Count - 1);
            //use a random seed to decide the final land type selection bassed on % chances
            int seed = Random.Range(0, 100);
            if (seed < 80 && neighborTypes.Count > 0)
            {
                newTile = neighborTypes[winner];
            }
            else if (seed < 87)
            {
                newTile = 0;//grass
            }
            else if (seed < 90)
            {
                newTile = 1;//sand
            }
            else if (seed < 95)
            {
                newTile = 2;//stone
            }
            else
            {
                newTile = 3;//water
            }
        }
        //make sure this is not an edge
        //Note: Even if this is an edge tile, it will still has a newTile type for use if it ever gets explored. 
        //neighbors can see the newTile type aswell.
        if (location.x == 0 || location.x == map.width || location.z == map.length)
        {//is this and edge tile?
            //force the tile to be an edge.            
            cube = (Transform)Instantiate(map.Land_Edge);
            //entryCost = int.MaxValue; // not sure if i want the pathing to be able to use the edges
            //visited = true;
        }
        else
        {
            //build tile of newTile type
            switch (tileType)
            {
                case 0: cube = (Transform)Instantiate(map.LandTypes[newTile]); break;
                case 1: cube = (Transform)Instantiate(map.Objects[newTile]); break;
            }
        }
        //make tile a child of this GameObject
        if (cube != null)
        {
            cube.transform.parent = transform;
            cube.transform.rotation = transform.rotation;
            transform.position = location;            
            //Debug.Log("Child added to " + this.name);
        }
        entryCost = (newTile * 4) + 1;
        if (autoExpand)
        {
            expand();
        }       
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// setup a spicific type of worldCell. If loc vector is null, then this is a mobile type and location will have to be set elsewhere
	public worldCell setup(Vector3 loc, int tileNumber, bool expand, int tileSet){
        location = loc;
        newTile = tileNumber;
		forced = true;
        tileType = tileSet;
        autoExpand = expand;
		Debug.Log("world cell setup run: " + loc);
		return this;
	}

	// setup that leaves the type open.
	public worldCell setup(Vector3 loc, bool expand){
		location = loc;
		forced = false;
        tileType = 0;
        autoExpand = expand;
		return this;
	}

	// Expand the explored land to map bounds
	// expand() will also link all touching tiles to this one.
	private void expand(){

		//Randomize the direction order. this helps the map look more natural
		List<Vector3> random = new List<Vector3>();
		foreach(Vector3 dir in map.directions){
			int temp = Random.Range(0,2);
			//Debug.Log(""+temp);
			switch(temp){
			case 0: random.Insert(0, dir); break;
			case 1: random.Add(dir); break;
			default: break;
			}
		}

		//Create and link neighboring tiles.
		foreach (Vector3 dir in random) {
			Vector3  modifiedLocation = (location + dir);
			if(modifiedLocation.x <= map.width && modifiedLocation.x >= 0 && modifiedLocation.z <= map.length && modifiedLocation.z >= 0){
				GameObject neighbor = map.get (modifiedLocation);
				if(neighbor == null){
					//make and link new tile
					neighbors[map.dirToInt(dir)] = new GameObject ("Tile " + modifiedLocation.ToString ()).AddComponent<worldCell> ().setup (modifiedLocation, true);
				}
				else{
					//link existing tile
					neighbors[map.dirToInt(dir)] = neighbor.GetComponent<worldCell>();
				}
			}
		}
	}

    public int CompareTo(worldCell cell){
        if (cell == null)
        {
            return 1;
        }
        return costPathToHere.CompareTo(cell.costPathToHere);
    }

    public bool Equals(worldCell cell)
    {
        if (cell == null) return false;
        return (this.location.Equals(cell.location));
    }
}
