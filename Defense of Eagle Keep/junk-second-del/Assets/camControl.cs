﻿using UnityEngine;
using System.Collections;

public class camControl : MonoBehaviour {

	public int speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.DownArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.down * speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.UpArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.up * speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.LeftArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.left * speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.right * speed * Time.deltaTime);
		}
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Enter pressed");
            pathFind.findPath();
        }
        if (Input.GetKeyDown(KeyCode.Backslash))
        {
            Debug.Log("\\ pressed");
            pathFind.clearPath();
        }
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            Debug.Log("Shift pressed");
            map.attach(map.get(new Vector3(Random.Range(1,map.width), 0, Random.Range(0,map.length))).GetComponent<worldCell>(), new GameObject("Tower").AddComponent<worldCell>().setup(Vector3.zero, 3, false, 1), Vector3.up);
        }
	}
}
