﻿using UnityEngine;
using System.Collections;

public class map : MonoBehaviour {

	public int inWidth;
	public static int width;

	public int inLength;
	public static int length;

    public Vector3 inStart;
    public static Vector3 start;

	public static Transform Land_Edge;// Land_Edge is not part of LandTypes because it is a special case. I dont want Land_Edges in the midle of my map.
	public static Transform[] LandTypes = new Transform[4];// Array of avalible land types;
    public static Transform[] Objects = new Transform[4];
	public static GameObject baseGO;
	public static Vector3[] directions = new Vector3[4]{Vector3.forward/*0*/, Vector3.left/*1*/, Vector3.back/*2*/, Vector3.right/*3*/};

	// Use this for initialization
	void Start () {
		width = inWidth;
		length = inLength;
        start = inStart;
		//setup avalible land types
		GameObject grass = Resources.Load("Land_Grass") as GameObject;
		LandTypes[0] = grass.transform;
		GameObject sand = Resources.Load("Land_Sand") as GameObject;
		LandTypes[1] = sand.transform;
		GameObject stone = Resources.Load("Land_Stone") as GameObject;
		LandTypes[2] = stone.transform;
		GameObject water = Resources.Load("Water_Basic") as GameObject;
		LandTypes[3] = water.transform;
		//setup edge type
		GameObject edge = Resources.Load("Land_Edge") as GameObject;
		Land_Edge = edge.transform;
        //setup objects
        GameObject road_straight = Resources.Load("Object_Road_Straight") as GameObject;
        Objects[0] = road_straight.transform;
        GameObject road_corner = Resources.Load("Object_Road_Corner") as GameObject;
        Objects[1] = road_corner.transform;
        GameObject castle = Resources.Load("Object_Castle") as GameObject;
        Objects[2] = castle.transform;
        GameObject tower = Resources.Load("Object_Tower_Round") as GameObject;
        Objects[3] = tower.transform;
		//start map build
		worldCell home = new GameObject ("Tile " + start.ToString()).AddComponent<worldCell>().setup (start, 0, true, 0);//this 0 forces the first tiles to be grass
        attach(home, new GameObject("Castle " + start.ToString()).AddComponent<worldCell>().setup(new Vector3(9,9,9), 2, false, 1), Vector3.up);
        //home.neighbors[4] = new GameObject("Castle " + start.ToString()).AddComponent<worldCell>().setup(start + Vector3.up, 2, false, 1);
        //home.neighbors[4].neighbors[5] = home;    start + Vector3.up, 2, false, 1
	}
	
	// Update is called once per frame
	void Update () {
	}

	//A good way to get a land spot by location.
	public static GameObject get(Vector3 location){
		return GameObject.Find("Tile " + location.ToString ());
	}

	//turn direction in ints used from neighbors arrays. If only I could index arrays with Vector3s.
	public static int dirToInt(Vector3 dir){
		Vector3 direction = dir.normalized;
        //Debug.Log(dir +  " normalized to " + direction);
        switch (direction.ToString()) {
			case "(0.0, 0.0, 1.0)": return 0;//forward
			case "(1.0, 0.0, 0.0)": return 1;//left
			case "(0.0, 0.0, -1.0)": return 2;//back
			case "(-1.0, 0.0, 0.0)": return 3;//right
            case "(0.0, 1.0, 0.0)": return 4;//up
            case "(0.0, -1.0, 0.0)": return 5;//down
            case "(0.7, 0.0, 0.7)": return 6;//forward_left
            case "(0.7, 0.0, -0.7)": return 7;//back_left
            case "(-0.7, 0.0, 0.7)": return 8;//forward_right
            case "(-0.7, 0.0, -0.7)": return 9;//back_right

			default: return -1;//should not happen, but you never know.
		}
	}

    //attach CellA to CellB in the direction from CellA to CellB
    public static bool attach(worldCell CellA, worldCell CellB, Vector3 direction){
        if(CellA != null && CellB != null && direction != null){
            int dir = dirToInt(direction);
            int revDir = dirToInt(direction * -1);
            if(dir >= 0 && dir <=5){//valid attach directions;
                CellA.neighbors[dir] = CellB;
                CellB.neighbors[revDir] = CellA;
                if (direction == Vector3.up)
                {
                    CellB.location = CellA.location + direction;
                }                
                return true;
            }
        }
        return false;
    }
}
