﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class pathFind : MonoBehaviour {

	static List<worldCell> openset = new List<worldCell>();
    static List<Vector3> path = new List<Vector3>();

    static worldCell castle;
    static worldCell entryPoint;

    static int totalCost;

    static worldCell start;

    static DateTime t;

    pathFind pf;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void findPath(){
        start = map.get(map.start).GetComponent<worldCell>();
        castle = start.neighbors[4];
        
        int I = 1000;//used to shup down the loop if it goes on to long. This number should be close to the number of tiles on the map.

        clearPath();// does nothing if no path exists.
        openset.Add(start);
                
        //uses Dijkstra's algorithm to find the best path.
        while (openset[0] != null && openset[0].location.z < map.length-1)
        {
            visit(openset[0]);
            openset.Sort();
            I--;
            if (I < 0)//force this loop to stop if their is not path
            {
                Debug.Log("Force break I = " + I);
                openset[0] = null;
            }
            //Debug.Log("looping...end I = " + I + " open set size = " + openset.Count + " openset[0] = " + openset[0].name);
		}
        Debug.Log("Path found");
        buildPath(openset[0]);
        markPath();

	}

    //the visit step of Dijkstra's algorithm. Should always be visiting the closest option.
    static void visit(worldCell cell){
        int currentCost = cell.costPathToHere;
        foreach(worldCell nextCell in cell.neighbors){
            if (nextCell != null && nextCell.visited == false)
            {
                int nextCost = currentCost + nextCell.entryCost;
                if (nextCost < currentCost)//catch int overflow
                {
                    nextCost = int.MaxValue;
                }
                if (nextCost < nextCell.costPathToHere)
                {
                    nextCell.costPathToHere = nextCost;
                }
                if (openset.Contains(nextCell) == false)//Only add this sell to open set if it is not already there.
                {
                    openset.Add(nextCell);
                }          
            }
        }        
        cell.visited = true;
        openset.Remove(cell);//once a cell is visited it must be removed from the open set.
    }

    //trace path back
    static void buildPath(worldCell cell)
    {
        if (cell.Equals(start))//break recursion here
        {
            return;
        }
        int cost = int.MaxValue;
        
        ////////////////////////////////////////////Vector3 modifiedLocation = cell.location + Vector3.up;     
        // find the shortest path into this cell
        worldCell temp = null;
        foreach (worldCell nextCell in cell.neighbors)
        {
            if (nextCell != null && nextCell.costPathToHere < cost)
            {
                cost = nextCell.costPathToHere;
                temp = nextCell;
            }
        }
        if (temp != null)//if there is a next path...there should be.
        {
            path.Add(cell.location + Vector3.up);
            buildPath(temp);
        }
    }

    static void markPath()
    {
        worldCell newCell = null;
        worldCell oldCell = null;
        Vector3 inDirection;
        Vector3 outDirection;
        for (int I = 0; I < path.Count; I++)
        {
            if (I == 0)//first path will always be the foward. Also, no linking is needed yet
            {
                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], 0, false, 1);
                oldCell = newCell;
            }
            else
            {
                inDirection = path[I] - path[I - 1];
                if(I != path.Count-1){//if this is not the last path
                    outDirection = path[I + 1] - path[I];
                }
                else
                {
                    outDirection = map.start + Vector3.up - path[I];
                    castle.transform.LookAt(path[I]);//set castle to face the path
                }
                if (inDirection == outDirection)//make a staight path(tile 0 from set 1)
                {
                    newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], 0, false, 1);
                    newCell.transform.LookAt(outDirection);
                }
                else//make a corner path(tile 1 from set 1)
                {
                    newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], 1, false, 1);
                    switch (map.dirToInt(inDirection - outDirection))// Rotate the courner tile to meet up with the rest of the path.
                    {
                        case 6: newCell.transform.LookAt(Vector3.back); break;
                        case 7: newCell.transform.LookAt(Vector3.left); break;
                        case 8: newCell.transform.LookAt(Vector3.right); break;
                        case 9: newCell.transform.LookAt(Vector3.forward); break;
                    }
                }
                //link the paths together. Not that with paths, the forward neigher is always the next path, not always the forward direction.
                map.attach(newCell, oldCell, Vector3.forward);
                //prep for next step
                oldCell = newCell;
            }
            //link path to the land tile below it.
            map.attach(newCell, map.get(newCell.location + Vector3.down).GetComponent<worldCell>(), Vector3.down);
        }
        //At the very end, link the castle to the end of the path. Possible remove this if i can make the castle part of the path. not worth the time right now
        map.attach(castle, newCell, Vector3.forward);
    }


    // remove the current path
    public static void clearPath()
    {
        if (castle.neighbors[0] != null)
        {
            worldCell oldPath = castle.neighbors[0];
            worldCell temp;
            resetVisited(castle.neighbors[5]);
            path.Clear();
            openset.Clear();
            while (oldPath != null)
            {
                temp = oldPath.neighbors[0];
                Destroy(oldPath.transform.gameObject);
                oldPath = temp;
            }
            t = DateTime.Now;
        }
        start.costPathToHere = 0;
    }

    //Undoe all the Dijkstra calculation to get ready for finding a new path
    private static void resetVisited(worldCell cellToReset)
    {
        cellToReset.visited = false;
        foreach (worldCell neighbor in cellToReset.neighbors)
        {
            if (neighbor != null)
            {
                neighbor.costPathToHere = int.MaxValue;
                if (neighbor.visited == true)
                {
                    resetVisited(neighbor);
                }
            }            
        }
    }
}
