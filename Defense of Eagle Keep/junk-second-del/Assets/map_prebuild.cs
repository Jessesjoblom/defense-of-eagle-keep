﻿using UnityEngine;
using System.Collections;

public class map_prebuild : MonoBehaviour {

    public int inWidth;
    public static int width;

    public int inLength;
    public static int length;

    public Vector3 inStartt;
    public static Vector3 start;

    public static Transform Land_Edge;// Land_Edge is not part of LandTypes because it is a special case. I dont want Land_Edges in the midle of my map.
    public static Transform[] LandTypes = new Transform[4];// Array of avalible land types;
    public static Transform[] Objects = new Transform[3];
    public static GameObject baseGO;
    public static Vector3[] directions = new Vector3[4] { Vector3.forward/*0*/, Vector3.left/*1*/, Vector3.back/*2*/, Vector3.right/*3*/};

    // Use this for initialization
    void Start()
    {
        map.width = inWidth;
        map.length = inLength;
        map.start = inStartt;
        width = inWidth;
        length = inLength;
        start = inStartt;
        //setup avalible land types
        GameObject grass = Resources.Load("Land_Grass") as GameObject;
        map.LandTypes[0] = grass.transform;
        GameObject sand = Resources.Load("Land_Sand") as GameObject;
        map.LandTypes[1] = sand.transform;
        GameObject stone = Resources.Load("Land_Stone") as GameObject;
        map.LandTypes[2] = stone.transform;
        GameObject water = Resources.Load("Water_Basic") as GameObject;
        map.LandTypes[3] = water.transform;
        //setup edge type
        GameObject edge = Resources.Load("Land_Edge") as GameObject;
        map.Land_Edge = edge.transform;
        //setup objects
        GameObject road_straight = Resources.Load("Object_Road_Straight") as GameObject;
        map.Objects[0] = road_straight.transform;
        GameObject road_corner = Resources.Load("Object_Road_Corner") as GameObject;
        map.Objects[1] = road_corner.transform;
        GameObject castle = Resources.Load("Object_Castle") as GameObject;
        map.Objects[2] = castle.transform;
        //start map build
        //worldCell home = new GameObject("Tile " + start.ToString()).AddComponent<worldCell>().setup(start, 0, true, 0);//this 0 forces the first tiles to be grass
        Debug.Log("start = " + map.start + " inStartt = " + inStartt);
        worldCell home = get(map.start).GetComponent<worldCell>();
        //home.neighbors[4] = new GameObject("Castle " + start.ToString()).AddComponent<worldCell>().setup(start + Vector3.up, 2, false, 1);
    }

    // Update is called once per frame
    void Update()
    {
    }

    //A good way to get a land spot by location.
    public static GameObject get(Vector3 location)
    {
        return GameObject.Find("Tile " + location.ToString());
    }

    //turn direction in ints used from neighbors arrays. If only I could index arrays with Vector3s.
    public static int dirToInt(Vector3 dir)
    {
        Vector3 direction = dir.normalized;
        switch (direction.ToString())
        {
            case "(0.0, 0.0, 1.0)": return 0;//forward
            case "(1.0, 0.0, 0.0)": return 1;//left
            case "(0.0, 0.0, -1.0)": return 2;//back
            case "(-1.0, 0.0, 0.0)": return 3;//right
            case "(0.0, 1.0, 0.0)": return 4;//up
            case "(0.0, -1.0, 0.0)": return 5;//down

            default: return -1;//should not happen, but you never know.
        }
    }
}