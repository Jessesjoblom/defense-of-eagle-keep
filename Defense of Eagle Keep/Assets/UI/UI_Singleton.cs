﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UI_Singleton : MonoBehaviour {

    private static UI_Singleton singleton;
    //private map.cellType selectedType;
    private int selected;
    private Slider health;
    private Text moneyText;
    private Text waveText;
    //private int money;
    private GameObject end;
    private GameObject menu;
    private bool menuActive;
    private float clickCD;


	// Use this for initialization
	void Start () {
        singleton = this;
        selected = -1;

        health = GameObject.Find("Keep_Health").GetComponent<Slider>();
        moneyText = GameObject.Find("Resources_Money").GetComponent<Text>();
        waveText = GameObject.Find("Wave_Text").GetComponent<Text>();
        displayHealth(100, 100);
        displayMoney(economyControl.getSingleton().getBalance());
        end = GameObject.Find("Game_Over");
        end.gameObject.SetActive(false);
        menu = GameObject.Find("Menu");
        menu.gameObject.SetActive(false);
        menuActive = false;
        clickCD = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if(clickCD > 0)
        {
            clickCD -= Time.deltaTime;
            //Debug.Log("click suppressed");
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            clickCD = .1F;
            Debug.Log("Left Click");
            Ray ray;
            RaycastHit hit;
            worldCell cell;
            worldCell tower;
            Transform tran;
            int cost;
            
            ray = Camera.allCameras[1].ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))//check for new tower selection
            {
                tran = hit.transform;
                cell = tran.GetComponent<worldCell>();
                while (cell == null && tran != null)
                {
                    tran = tran.parent;
                    cell = tran.GetComponent<worldCell>();
                }
                if (cell.neighbors[5] == null && cell.tileSet == map.cellType.Objects)
                {
                    Debug.Log("unit selected");
                    selected = cell.tile;
                }
            }   
            else//if not new tower, try to place existion tower;
            {
                if (selected < 0)
                {
                    Debug.Log("No tower type selected.");
                    return;         
                }
                switch (selected)
                {
                    case 0: Debug.Log("Invalid object \"Keep\" selected."); return;
                    case 1: cost = 25; if(economyControl.getSingleton().isWithdrawPossible(cost)) { break; } else { Debug.Log(cost + " money not avalible"); return; }
                    case 2: cost = 15; if(economyControl.getSingleton().isWithdrawPossible(cost)) { break; } else { Debug.Log(cost + " money not avalible"); return; }
                    default: Debug.Log("selection not recognized"); return;
                }
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000))
                {
                    tran = hit.transform;
                    cell = tran.GetComponent<worldCell>();
                    while (cell == null && tran != null)
                    {
                        tran = tran.parent;
                        cell = tran.GetComponent<worldCell>();
                    }
                    if (cell != null && cell.tileSet == map.cellType.LandBasic && cell.tile != 3 && cell.neighbors[4] == null)// 3 = water. Cant place on water
                    {
                        economyControl.getSingleton().withdraw(cost);
                        tower = new GameObject("Object " + cell.location + Vector3.up).AddComponent<worldCell>().setup(Vector3.zero, selected, map.cellType.Objects, true, false);
                        map.attach(cell, tower, Vector3.up);
                        tower.entryCost = int.MaxValue;
                        tower.Mask = LayerMask.NameToLayer("Ignore Weapon");
                        battle.getBattleSingleton().registerTower(tower);    
                    }
                    else if (cell != null && cell.tileSet == map.cellType.Path && (cell.tile == 0 || cell.tile == 1))//path type 0 and 1 are the level paths
                    {                            
                        if (battle.getBattleSingleton().EnemyList.Count == 0)
                        {
                            int tempCost = cell.neighbors[5].entryCost;//save this entry cost because we will have to replace it after checking to see if a path can be found without this cell
                            cell.neighbors[5].entryCost = int.MaxValue;
                            if (pathFind.findPath())//if a path exits not use the cell
                            {
                                economyControl.getSingleton().withdraw(cost);
                                tower = new GameObject("Object " + cell.location).AddComponent<worldCell>().setup(Vector3.zero, selected, map.cellType.Objects, true, false);
                                map.attach(cell.neighbors[5], tower, Vector3.up);
                                tower.entryCost = int.MaxValue;
                                tower.Mask = LayerMask.NameToLayer("Ignore Weapon");
                                battle.getBattleSingleton().registerTower(tower);
                                cell.neighbors[5].entryCost = tempCost;//restore cells entry cost
                            }
                            else
                            {
                                cell.neighbors[5].entryCost = tempCost;//restore cells entry cost
                                pathFind.findPath();
                            }
                        }
                    }
                }
            }
        }
	}

    public static UI_Singleton getSingleton(){
        if (singleton == null)
        {
            singleton = new UI_Singleton();
        }
        return singleton;
    }

    public void begin()
    {

        worldCell tower_1 = GameObject.Find("Inventory_Tower_Round").GetComponent<worldCell>();
        Vector2 pos_1 = tower_1.transform.position;
        Quaternion rot_1 = tower_1.transform.rotation;
        tower_1.renderTile();
        tower_1.transform.position = pos_1;
        tower_1.transform.rotation = rot_1;
        while (tower_1.Cube == null) { }//spin till cube is ready
        map.setLayer(tower_1.transform, LayerMask.NameToLayer("UI"));

        worldCell tower_2 = GameObject.Find("Inventory_Tower_Wood").GetComponent<worldCell>();
        Vector2 pos_2 = tower_2.transform.position;
        Quaternion rot_2 = tower_2.transform.rotation;
        tower_2.renderTile();
        tower_2.transform.position = pos_2;
        tower_2.transform.rotation = rot_2;
        while (tower_2.Cube == null) { }//spin till cube is ready
        map.setLayer(tower_2.transform, LayerMask.NameToLayer("UI"));
    }

    public int Selected
    {
        get
        {
            return selected;
        }

        set
        {            
            selected = value;
            Debug.Log("selection accepted");
        }
    }

    public void displayHealth(float Current, float Max)
    {
        Debug.Log("Keep hit! Health: " + Current + "/" + Max + ".");
        health.value = (1000 / Max) * Current;
        GameObject.Find("Keep_Current_Health_Text").GetComponent<UnityEngine.UI.Text>().text = Current.ToString();
        GameObject.Find("Keep_C/M_Text").GetComponent<UnityEngine.UI.Text>().text = Current + "/" + Max;
    }

    public void displayMoney(int money)
    {
        moneyText.text = money.ToString();
    }

    public void displayWave(int wave)
    {
        waveText.text = wave.ToString();
    }

    public void menuToggle()
    {
        menuActive = !menuActive;
        menu.SetActive(menuActive);
    }

    public void gameOver()
    {
        Debug.Log("Game Over");
        end.gameObject.SetActive(true);
        map.setLayer(GameObject.Find("Inventory_Tower_Round").transform, LayerMask.NameToLayer("Default"));
        map.setLayer(GameObject.Find("Inventory_Tower_Wood").transform, LayerMask.NameToLayer("Default"));
        GameObject.Find("End").GetComponent<Image>().CrossFadeAlpha(256F, 2F, false);
        GameObject button = Instantiate(Resources.Load("UI/Button_Restart")) as GameObject;//place restart button
        button.transform.parent = GameObject.Find("End").transform;
        button.transform.localPosition = new Vector3(0,-220,0);
        button.transform.localScale = new Vector3(1, 1, 1);
        button.transform.localRotation = new Quaternion(0,0,0,0);
        button.GetComponent<Button>().onClick.AddListener(() => restartGame());
    }

    public void gameVictory()
    {
        Debug.Log("Game Victory");
        end.gameObject.SetActive(true);
        map.setLayer(GameObject.Find("Inventory_Tower_Round").transform, LayerMask.NameToLayer("Default"));
        map.setLayer(GameObject.Find("Inventory_Tower_Wood").transform, LayerMask.NameToLayer("Default"));
        //GameObject.Find("End").GetComponent<Image>().CrossFadeColor(Color.blue, 2F,false,false);
        //GameObject.Find("End").GetComponent<Image>().CrossFadeAlpha(256F, 2F, false);
        GameObject.Find("Game_Over_Title").GetComponent<Text>().text = "You Win!";
        GameObject button = Instantiate(Resources.Load("UI/Button_Restart")) as GameObject;//place restart button
        button.transform.parent = GameObject.Find("End").transform;
        button.transform.localPosition = new Vector3(0, -220, 0);
        button.transform.localScale = new Vector3(1, 1, 1);
        button.transform.localRotation = new Quaternion(0, 0, 0, 0);
        button.GetComponent<Button>().onClick.AddListener(() => restartGame());
    }

    public void restartGame()
    {
        Debug.Log("Restart game");
        GameObject.Find("Game_Over_Title").GetComponent<Text>().text = "Game Over!";
        end.gameObject.SetActive(false);
                
        economyControl.getSingleton().reset();
        battle.getBattleSingleton().reset();
        map.reset();
        Destroy(GameObject.Find("Button_Restart(Clone)"));//reseting the ui
        map.setLayer(GameObject.Find("Inventory_Tower_Round").transform, LayerMask.NameToLayer("UI"));//reseting the ui
        map.setLayer(GameObject.Find("Inventory_Tower_Wood").transform, LayerMask.NameToLayer("UI"));//reseting the ui
        //GameObject.Find("End").GetComponent<Image>().color = Color.black;
        GameObject.Find("End").GetComponent<Image>().CrossFadeAlpha(1F, 2F, false);//reseting the ui
        
        
        
    }

    public void exitGame()
    {
        Debug.Log("Exit Game");
        Application.Quit();
    }
}
