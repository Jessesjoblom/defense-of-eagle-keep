﻿using UnityEngine;
using System.Collections;

public class camControl : MonoBehaviour {

	public int speed;
    public static Material mat;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.DownArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.down * speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.UpArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.up * speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.LeftArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.left * speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			GameObject.Find ("Main Camera").transform.Translate (Vector3.right * speed * Time.deltaTime);
		}
        /*if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Enter pressed");
            pathFind.findPath();
        }
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Right Click");
            if (pathFind.pathEnd != null)
            {
                GameObject go = Instantiate(Resources.Load("Unit_Temp")) as GameObject;
                go.transform.name = "Unit " + Random.Range(0,100000);
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Left Click");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            worldCell cell;
            Transform tran;
            if(Physics.Raycast(ray, out hit, 1000)){                
                tran = hit.transform;
                cell = tran.GetComponent<worldCell>();
                while (cell == null && tran != null)
                {
                    tran = tran.parent;
                    cell = tran.GetComponent<worldCell>();
                }
                if (cell != null && cell.tileSet == map.cellType.LandBasic && cell.tile != 3 && cell.neighbors[4] == null)
                {
                    map.attach(cell, new GameObject("Object " + cell.location + Vector3.up).AddComponent<worldCell>().setup(Vector3.zero, 1, map.cellType.Objects, true, false), Vector3.up);
                    cell.entryCost = int.MaxValue;
                }
                else if (cell != null && cell.tileSet == map.cellType.Path && (cell.tile == 0 || cell.tile == 1))
                {
                    int tempCost = cell.neighbors[5].entryCost;
                    cell.neighbors[5].entryCost = int.MaxValue;
                    if (pathFind.findPath())
                    {
                        map.attach(cell.neighbors[5], new GameObject("Object " + cell.location).AddComponent<worldCell>().setup(Vector3.zero, 1, map.cellType.Objects, true, false), Vector3.up);
                    }
                    else
                    {
                        cell.neighbors[5].entryCost = tempCost;
                        pathFind.findPath();
                    }
                }
            }
        }*/
	}
}
