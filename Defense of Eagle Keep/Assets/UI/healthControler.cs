﻿using UnityEngine;
using System.Collections;

public class healthControler : MonoBehaviour {

	public float currentHealth;
    public float maxHealth;

    private UnityEngine.UI.Slider slider;


    // Use this for initialization
	void Start () {
        currentHealth = 50;
        maxHealth = 100;
        slider = GameObject.Find("Keep_Health").GetComponent<UnityEngine.UI.Slider>();
        slider.value = (1000/maxHealth) * currentHealth;
        GameObject.Find("Keep_Current_Health_Text").GetComponent<UnityEngine.UI.Text>().text = currentHealth.ToString();
        GameObject.Find("Keep_Status_Text").GetComponent<UnityEngine.UI.Text>().text = currentHealth + "/" + maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
