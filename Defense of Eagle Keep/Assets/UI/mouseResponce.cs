﻿using UnityEngine;
using System.Collections;

public class mouseResponce : MonoBehaviour {

    Vector3 location;
    Vector3 size;
    worldCell parentCell;
    // Use this for initialization
	void Start () {
        {
            Transform tran = transform;
            location = transform.localPosition;
            size = transform.localScale;
            parentCell = tran.GetComponent<worldCell>();
            while (parentCell == null && tran != null)
            {
                tran = tran.parent;
                parentCell = tran.GetComponent<worldCell>();
            }
        }        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseEnter()
    {
        if (map.locked == true && parentCell.neighbors[4] == null)
        {
            transform.localPosition = location + Vector3.up * .2F;
            transform.localScale = size * 1.1F;
        }        
    }

    void OnMouseExit()
    {
        transform.localPosition = location;
        transform.localScale = size;        
    }

    void onMouseDown()
    {
        Debug.Log("unit selected");
        UI_Singleton.getSingleton().Selected = 1;
    }
}
