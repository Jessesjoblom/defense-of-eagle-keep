﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class pathFind : MonoBehaviour {

    static worldCell start;
    static worldCell end;

    public static worldCell pathStart;
    public static worldCell pathEnd;

    static List<worldCell> openSet = new List<worldCell>();
    //static List<worldCell> path = new List<worldCell>();

    public static bool findPath()
    {
        //find path start
        start = map.get(map.start).GetComponent<worldCell>();
        pathStart = map.Castle; //this is the Castle

        //if (pathStart.neighbors[0] != null)// remove this late. this is for path testing
        //{                                  //
            clearPath();
        //    return;                        //
        //}                                  //     
        
        start.costPathToHere = 0;
        openSet = new List<worldCell>();
        openSet.Add(start);
        if (visit(start) == true)
        {
            buildPath(end, null, 2);
        
            //find pathend
            pathEnd = end.getTop();
            if (pathEnd.neighbors[4] != null)
            {
                pathEnd = pathEnd.neighbors[4];
            }
            return true;
        }
        return false;
    }

    //the visit step of Dijkstra's algorithm. Should always be visiting the closest option.
    private static bool visit(worldCell cell)
    {

        int tempCostPathToNeighbor;
        worldCell neighbor;
        worldCell top;
        //visit cell and remove it from the open set.
        cell.visited = true;
        openSet.Remove(cell);
        if (cell.location.z >= map.length)
        {
            end = cell;
            return true;
        }
        else if (cell.costPathToHere == int.MaxValue)
        {
            Debug.Log("Path cost to high.");
            return false;
        }
        for (int direction = 0; direction < 4; direction++)
        {
            tempCostPathToNeighbor = cell.costPathToHere;            
            if (cell.neighbors[direction] != null && cell.neighbors[direction].visited == false)
            {
                neighbor = cell.neighbors[direction];//improve readablitly
                top = neighbor; //prime the loop
                while (top.neighbors[4] != null)//loop generates higher costs for greater land elivation
                {
                    tempCostPathToNeighbor += top.neighbors[4].entryCost;
                    top = top.neighbors[4];
                }
                tempCostPathToNeighbor += neighbor.entryCost;
                //detect overflow
                if (tempCostPathToNeighbor < cell.costPathToHere)
                {
                    tempCostPathToNeighbor = int.MaxValue;
                }
                //check to see if connection is a better option
                if (tempCostPathToNeighbor < neighbor.costPathToHere)
                {
                    neighbor.costPathToHere = tempCostPathToNeighbor;
                }
                //add neighbor to open set if needed
                if (openSet.Contains(neighbor) != true)
                {
                    openSet.Add(neighbor);
                }
            }
        }
        //sort set to make sure that closest cell in at index 0
        if (openSet.Count > 0)
        {
            openSet.Sort();
            return visit(openSet[0]);
        }
        return false;
    }

    private static void buildPath(worldCell cell, worldCell lastPath, int dir)
    {
        worldCell top;
        if (cell.Equals(start))
        {
            top = start.getTop().neighbors[4];
            map.attach(top, lastPath, Vector3.forward);
            //pathStart.transform.LookAt(new Vector3(lastPath.location.x, top.location.y, lastPath.location.z));
            pathStart.orientation = map.intToDir(dir) * -1;
            return;
        }        
        
        int cost = int.MaxValue;
        worldCell next = new worldCell();
        int nextDir = 0;
        for (int direction = 0; direction < 4; direction++)
        {
            if (cell.neighbors[direction] != null && cell.neighbors[direction].costPathToHere < cost)
            {
                cost = cell.neighbors[direction].costPathToHere;
                next = cell.neighbors[direction];
                nextDir = direction;
            }
        }

        top = cell.getTop();
        worldCell path;        
        if (dir == nextDir)//true if we are going to use a strait path
        {
            if (top.tileSet == map.cellType.LandBasic)//level path
            {
                path = new GameObject("Path " + (top.location + Vector3.up).ToString()).AddComponent<worldCell>().setup(top.location + Vector3.up, 0, map.cellType.Path, true, false);
                path.orientation = map.intToDir(dir);
            }
            else//slanted path
            {
                if (dir == map.dirToInt(top.orientation) || dir == map.dirToInt(top.orientation * -1))
                {
                    path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 2, map.cellType.Path, true, false);
                    path.orientation = top.orientation;
                    Debug.Log("this path should be a long slant " + path);
                }
                else
                {
                    path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 3, map.cellType.Path, true, false);
                    path.orientation = top.orientation;
                    Debug.Log("this path should be a long slant " + path);
                }
            }
        }
        else// corner paths
        {
            if (top.tileSet == 0)//level path
            {
                path = new GameObject("Path " + (top.location + Vector3.up).ToString()).AddComponent<worldCell>().setup(top.location + Vector3.up, 1, map.cellType.Path, true, false);
                switch (map.dirToInt(map.intToDir(dir) - map.intToDir(nextDir)))// Rotate the courner tile to meet up with the rest of the path.
                {
                    case 6: path.orientation = Vector3.back; break;
                    case 7: path.orientation = Vector3.left; break;
                    case 8: path.orientation = Vector3.right; break;
                    case 9: path.orientation = Vector3.forward; break;
                }
            }
            else//slanted path
            {
                path = new worldCell().setup(new Vector3(5,5,5));
                switch (map.dirToInt(map.intToDir(dir) - map.intToDir(nextDir)))// Rotate the corner tile to meet up with the rest of the path.
                {
                    case 6: 
                        switch (map.dirToInt(top.orientation))
                        {
                            case 0:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 7, map.cellType.Path, true, false);
                                path.orientation = Vector3.forward;
                                Debug.Log(path + " is a case 6.0");
                                break;
                            case 1:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 4, map.cellType.Path, true, false);
                                path.orientation = Vector3.left;
                                Debug.Log(path + " is a case 6.1");
                                break;
                            case 2:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 5, map.cellType.Path, true, false); 
                                path.orientation = Vector3.back;
                                Debug.Log(path + " is a case 6.2");
                                break;
                            case 3:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 6, map.cellType.Path, true, false); 
                                path.orientation = Vector3.right;
                                Debug.Log(path + " is a case 6.3");
                                break;
                        }
                    break;
                    case 7: 
                        switch (map.dirToInt(top.orientation))
                        {
                            case 0:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 4, map.cellType.Path, true, false);
                                path.orientation = Vector3.forward;
                                Debug.Log(path + " is a case 7.0");
                                break;
                            case 1:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 5, map.cellType.Path, true, false);
                                path.orientation = Vector3.left;
                                Debug.Log(path + " is a case 7.1");
                                break;
                            case 2:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 6, map.cellType.Path, true, false); 
                                path.orientation = Vector3.back;
                                Debug.Log(path + " is a case 7.2");
                                break;
                            case 3:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 7, map.cellType.Path, true, false); 
                                path.orientation = Vector3.right;
                                Debug.Log(path + " is a case 7.3");
                                break;
                        }
                    break;
                    case 8: 
                        switch (map.dirToInt(top.orientation))
                        {
                            case 0:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 6, map.cellType.Path, true, false); 
                                path.orientation = Vector3.forward;
                                Debug.Log(path + " is a case 8.0");
                                break;
                            case 1:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 7, map.cellType.Path, true, false); 
                                path.orientation = Vector3.left;
                                Debug.Log(path + " is a case 8.1");
                                break;
                            case 2:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 4, map.cellType.Path, true, false); 
                                path.orientation = Vector3.back;
                                Debug.Log(path + " is a case 8.2");
                                break;
                            case 3:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 5, map.cellType.Path, true, false); 
                                path.orientation = Vector3.right;
                                Debug.Log(path + " is a case 8.3");
                                break;
                        }
                    break;
                    case 9: 
                        switch (map.dirToInt(top.orientation))
                        {
                            case 0:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 5, map.cellType.Path, true, false);
                                path.orientation = Vector3.forward;
                                Debug.Log(path + " is a case 9.0");
                                break;
                            case 1:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 6, map.cellType.Path, true, false);
                                path.orientation = Vector3.left;
                                Debug.Log(path + " is a case 9.1");
                                break;
                            case 2:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 7, map.cellType.Path, true, false); 
                                path.orientation = Vector3.back;
                                Debug.Log(path + " is a case 9.2");
                                break;
                            case 3:
                                path = new GameObject("Path " + (top.location).ToString()).AddComponent<worldCell>().setup(top.location, 4, map.cellType.Path, true, false); 
                                path.orientation = Vector3.right;
                                Debug.Log(path + " is a case 9.3");
                                break;
                        }
                    break;
                }               
            }
        }
        map.attach(top, path, Vector3.up);
        //Debug.Log(path + " has an exit direction of " + nextDir);
        if (path != null)
        {
            path.gameObject.AddComponent<rail>().set(map.intToDir(nextDir));
        }
        else
        {
            Debug.LogError("There is a problem with path at " + top + ". dir: " + dir + " nextdir: " + nextDir);
        }
        if (lastPath != null)
        {
            map.attach(lastPath, path, Vector3.back);
        }
        buildPath(next, path, nextDir);
    }

    //remove the path cells are reset the map for an new path search
    public static void clearPath()
    {
        openSet.Clear();
        worldCell temp;
        if (pathStart.neighbors[0] != null)
        {
            worldCell oldPath = pathStart.neighbors[0];
            while (oldPath != null)
            {
                temp = oldPath.neighbors[0];
                oldPath.neighbors[5].neighbors[4] = null;
                Destroy(oldPath.transform.gameObject);
                oldPath = temp;
            }
        }
        resetVisited(start);
    }

    //Undoe all the Dijkstra calculation to get ready for finding a new path
    private static void resetVisited(worldCell cellToReset)
    {
        cellToReset.visited = false;
        for (int I = 0; I < 4; I++ )
        {
            if (cellToReset.neighbors[I] != null)
            {
                cellToReset.neighbors[I].costPathToHere = int.MaxValue;
                if (cellToReset.neighbors[I].visited == true)
                {
                    resetVisited(cellToReset.neighbors[I]);
                }
            }
        }
    }
    
    
    /*static List<worldCell> openset = new List<worldCell>();
    static List<Vector3> path = new List<Vector3>();

    static worldCell castle;
    static worldCell entryPoint;

    static int totalCost;

    static worldCell start;

    static DateTime t;

    pathFind pf;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void findPath(){
        start = map.get(map.start).GetComponent<worldCell>();
        castle = start.neighbors[4];
        
        int I = 1000;//used to shup down the loop if it goes on to long. This number should be close to the number of tiles on the map.

        clearPath();// does nothing if no path exists.
        openset.Add(start);
                
        //uses Dijkstra's algorithm to find the best path.
        while (openset[0] != null && openset[0].location.z < map.length-1)
        {
            visit(openset[0]);
            openset.Sort();
            I--;
            if (I < 0)//force this loop to stop if their is not path
            {
                Debug.Log("Force break I = " + I);
                openset[0] = null;
            }
            //Debug.Log("looping...end I = " + I + " open set size = " + openset.Count + " openset[0] = " + openset[0].name);
		}
        Debug.Log("Path found");
        buildPath(openset[0]);
        markPath();

	}

    //the visit step of Dijkstra's algorithm. Should always be visiting the closest option.
    static void visit(worldCell cell){
        int currentCost = cell.costPathToHere;
		worldCell topCell = null;
        //if (cell.tileSet == 1)
        //{
        //    //When a hill is involved there are only two valid directions for the path down and the highland direction.
        //    //this is because i do not want the paths to turn on hillsides.
        //}
        foreach(worldCell nextCell in cell.neighbors){
            if (nextCell != null && nextCell.visited == false)
            {
                //Limit the valid path direction on sland(1) tiles types
                if(cell.tileSet != 1 || (map.dirToInt(nextCell.location - cell.location) == 5 || map.dirToInt(nextCell.location - cell.location) == map.dirToInt(cell.orientation * -1))){				    
                    int nextCost = currentCost;
				    //paths can only use top most cells, going up in evelation costs a penalty.
				    //topCell = nextCell;
				    //while(topCell.neighbors[4] != null && topCell.location.y >= cell.location.y){
				    //	nextCost = nextCost + (topCell.entryCost*2);
	                //    if (nextCost < currentCost)//catch int overflow
	                //    {
	                //        nextCost = int.MaxValue;
	                //    }
				    //	if (nextCost < topCell.costPathToHere)
				    //	{
				    //		topCell.costPathToHere = nextCost;
				    //	}
				    //	topCell = topCell.neighbors[4];
				    //}
                    if (nextCell.neighbors[4] == null)
                    {
                        nextCost = nextCost + (nextCell.entryCost);
                    }
                    else
                    {
                        nextCost = nextCost + (nextCell.entryCost * 2);//make underground path not attractive
                    }                
				    if (nextCost < currentCost)//catch int overflow
				    {
					    nextCost = int.MaxValue;
				    }
                    if (nextCost < nextCell.costPathToHere)
                    {
                        nextCell.costPathToHere = nextCost;
                    }
                    if (openset.Contains(nextCell) == false)//Only add this cell to open set if it is not already there.
                    {
                        openset.Add(nextCell);
                    } 
                }
            }
        }        
        cell.visited = true;
        openset.Remove(cell);//once a cell is visited it must be removed from the open set.
    }

    //trace path back
    static void buildPath(worldCell cell)
    {
        if (cell.Equals(start))//break recursion here
        {
            return;
        }
        int cost = int.MaxValue;
        
        ////////////////////////////////////////////Vector3 modifiedLocation = cell.location + Vector3.up;     
        // find the shortest path into this cell
        worldCell temp = null;
        foreach (worldCell nextCell in cell.neighbors)
        {
            if (nextCell != null && nextCell.costPathToHere < cost)
            {
                cost = nextCell.costPathToHere;
                temp = nextCell;
            }
        }
        if (temp != null)//if there is a next path...there should be.
        {
            path.Add(cell.location + Vector3.up);
            buildPath(temp);
        }
    }

    static void markPath()
    {
        worldCell newCell = null;
        worldCell oldCell = null;
        Vector3 inDirection;
        Vector3 outDirection;
        for (int I = 0; I < path.Count; I++)
        {
            if (I == 0)//first path will always be the foward. Also, no linking is needed yet
            {
                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], 0, 4, true, false);
                oldCell = newCell;
                map.pathStart = newCell;
            }
            else
            {
                inDirection = path[I] - path[I - 1];
                if(I != path.Count-1){//if this is not the last path
                    outDirection = path[I + 1] - path[I];
                }
                else
                {
                    outDirection = map.start + Vector3.up - path[I];
                    castle.transform.LookAt(path[I]);//set castle to face the path
                }
                if (inDirection == outDirection)//make a staight path(tile 0 from set 1)
                {
                    newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], outDirection, 0, 4, true, false, null);
                    //newCell.transform.LookAt(outDirection);
                }
                else//make a change in direaction
                {
                    //newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], 1, 2, true, false);
                    switch (map.dirToInt(inDirection - outDirection))// Rotate the courner tile to meet up with the rest of the path.
                    {
                        case 6: newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.back, 1, 4, true, false, null); break;
                        case 7: newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.left, 1, 4, true, false, null); break;
                        case 8: newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.right, 1, 4, true, false, null); break;
						case 9: newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.forward, 1, 4, true, false, null); break;

						case 10: 
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 10 and goes in the forward direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.forward, 4, 4, true, false, null);
							}
							break;
						case 11:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 11 and goes in the back direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.back, 4, 4, true, false, null);
							}
							break;
						case 12:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 12 and goes in the forward direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.forward, 4, 4, true, false, null);
							}
							break;
						case 13:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 13 and goes in the back direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.back, 4, 4, true, false, null);
							}
							break;
						case 14:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 14 and goes in the right direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.right, 4, 4, true, false, null);
							}
							break;
						case 15:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 15 and goes in the right direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.right, 4, 4, true, false, null);
							}
							break;
						case 16:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 16 and goes in the left direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.left, 4, 4, true, false, null);
							}
							break;
						case 17:
							if(map.get (path[I]) == null)
							{
                                Debug.Log("slant at location " + path[I] + " is a case 17 and goes in the left direction");
                                newCell = new GameObject("Path " + path[I].ToString()).AddComponent<worldCell>().setup(path[I], Vector3.left, 4, 4, true, false, null);
							}
						break;
					}
                }
                //link the paths together. Note that with paths, the forward neigher is always the next path, not always the forward direction.
                //if(newCell != null){
					map.attach(newCell, oldCell, Vector3.forward);
					//link path to the land tile below it.
					map.attach(newCell, map.get(newCell.location + Vector3.down).GetComponent<worldCell>(), Vector3.down);
					//prep for next step
					oldCell = newCell;
				//}
            }            
        }
        //At the very end, link the castle to the end of the path. Possible remove this if i can make the castle part of the path. not worth the time right now
        map.attach(castle, newCell, Vector3.forward);
    }


    // remove the current path
    public static void clearPath()
    {
        if (castle.neighbors[0] != null)
        {
            worldCell oldPath = castle.neighbors[0];
            worldCell temp;
            resetVisited(castle.neighbors[5]);
            path.Clear();
            openset.Clear();
            while (oldPath != null)
            {
                temp = oldPath.neighbors[0];
                Destroy(oldPath.transform.gameObject);
                oldPath = temp;
            }
            t = DateTime.Now;
        }
        start.costPathToHere = 0;
    }

    //Undoe all the Dijkstra calculation to get ready for finding a new path
    private static void resetVisited(worldCell cellToReset)
    {
        cellToReset.visited = false;
        foreach (worldCell neighbor in cellToReset.neighbors)
        {
            if (neighbor != null)
            {
                neighbor.costPathToHere = int.MaxValue;
                if (neighbor.visited == true)
                {
                    resetVisited(neighbor);
                }
            }            
        }
    }*/
}
