﻿using UnityEngine;
using System.Collections;

public class rail : MonoBehaviour {

    public Vector3 center;
    public Vector3 exit;
    public worldCell cell;
    public rail nextRail;

    // Use this for initialization
	void Start () {
        //cell = transform.GetComponent<worldCell>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void set(Vector3 exitDirection)
    {
        cell = transform.GetComponent<worldCell>();        
        
        if (cell.tileSet == map.cellType.Objects && cell.tile == 0)//special case for castles. castles do not have an exit Direction
        {
            center = cell.location;
            exit = center + Vector3.up * 3;//should never go here
            return;
        }

        switch (cell.tile)
        {
            case 0:
            case 1:
                center = cell.location - new Vector3(0, (float)0.4, 0);
                exit = center + exitDirection * (float)0.5;
                break;
            case 2: 
                center = cell.location + new Vector3(0, (float)0.07, 0) + cell.orientation * (float)0.07;
                if (exitDirection == cell.orientation)
                {
                    exit = cell.location - new Vector3(0, (float)0.4, 0) + cell.orientation * (float)0.54;
                }
                else
                {
                    exit = cell.location + new Vector3(0, (float)0.6, 0) - cell.orientation * (float)0.47;
                }
                break;
            case 3: 
                center = cell.location + new Vector3(0, (float)0.07, 0) + cell.orientation * (float)0.07;
                exit = center + exitDirection * (float)0.5;
                break;
            case 4:
            case 5:
                center = cell.location + new Vector3(0, (float)0.07, 0) + cell.orientation * (float)0.07;
                if (exitDirection == cell.orientation)
                {
                    exit = cell.location - new Vector3(0, (float)0.4, 0) + cell.orientation * (float)0.54;
                }
                else
                {
                    exit = center + exitDirection * (float)0.5;
                }                
                break;
            case 6:
            case 7:
                center = cell.location + new Vector3(0, (float)0.07, 0) + cell.orientation * (float)0.07;
                if (exitDirection == cell.orientation *-1)
                {
                    exit = cell.location + new Vector3(0, (float)0.6, 0) - cell.orientation * (float)0.47; 
                }
                else
                {
                    exit = center + exitDirection * (float)0.5;
                }
                break;
        }
        //Debug.Log(cell + " has an exit direction of " + exitDirection);
        Debug.Log(cell + " has a center point: " + center + " and a exit point: " + exit + ".");
    }

    public Vector3 Center
    {
        get
        {
            return center;
        }
        
    }

    public Vector3 Exit
    {
        get
        {
            return exit;
        }        
    }

    public rail NextRail
    {
        get
        {
            if (nextRail == null)
            {
                if (cell.neighbors[2] != null)
                {
                    nextRail = cell.neighbors[2].transform.GetComponent<rail>();
                }
            }
            return nextRail;
        }
    }

    /*public bool getNext(unitControl unit)
    {
        if (unit.transform.position == center)
        {
            unit.To = exit;
            unit.From = unit.transform.position;
        }
        else if (unit.transform.position == exit)
        {
            unit.Rail = cell.neighbors[2].transform.GetComponent<rail>();
            return false;
        }
        else
        {
            unit.To = center;
            unit.From = unit.transform.position;
        }
        return true;
    }*/

    //modification of visitor pattern
    public void accept(unitControl unit)
    {
        unit.visit(this);
    }


}
