﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class map : MonoBehaviour {

	public int inWidth;
	public static int width;

	public int inLength;
	public static int length;

	public bool inSkipStart;
	public static bool skipStart;

	public static bool locked;
	static int lockFrameTimer;

    public Vector3 inStart;
    public static Vector3 start;
    public static worldCell Castle;

    private static bool expandUp;

	public static Transform Land_Edge;// Land_Edge is not part of LandTypes because it is a special case. I dont want Land_Edges in the midle of my map.
	public static Transform[] LandBasic = new Transform[4];// Tile set 0;
    public static Transform[] LandSlant = new Transform[4];// Tile set 1;
    public static Transform[] LandSlantCornerTrio = new Transform[4];// Tile set 2;
    public static Transform[] LandSlantCornerDuo = new Transform[4];// Tile set 3;
    public static Transform[] LandPeak = new Transform[4];// Tile set 4;
    public static Transform[] LandEdgeMask = new Transform[2];// Tile set 5;
    public static Transform[] Objects = new Transform[3];// Tile set 6;
    public static Transform[] Path = new Transform[8];// Tile set 7;
    public static Transform[] Projectile = new Transform[2];// not a tile set
	public static Vector3[] directions = new Vector3[6]{Vector3.forward/*0*/, Vector3.left/*1*/, Vector3.back/*2*/, Vector3.right/*3*/, Vector3.up/*4*/, Vector3.down/*5*/};
    public static worldCell pathStart;

    public enum cellType{LandBasic,LandSlant,LandSlantCornerTrio,LandSlantCornerDuo,LandPeak,LandEdgeMask,Objects,Path};

	// Use this for initialization
	void Start () {
		width = inWidth;
		length = inLength;
        start = inStart;
		skipStart = inSkipStart;
		unlock();
        ExpandUp = true;
			//setup avalible land types
		GameObject grass = Resources.Load("Land/Grass/Land_Grass") as GameObject;
        LandBasic[0] = grass.transform;
		GameObject sand = Resources.Load("Land/Sand/Land_Sand") as GameObject;
        LandBasic[1] = sand.transform;
        GameObject stone = Resources.Load("Land/Stone/Land_Stone") as GameObject;
        LandBasic[2] = stone.transform;
        GameObject water = Resources.Load("Land/Water/Land_Water") as GameObject;//say what!!!
        LandBasic[3] = water.transform;
		//setup the slant objects
        GameObject grass_slant = Resources.Load("Land/Grass/Land_Grass_Slant") as GameObject;
        LandSlant[0] = grass_slant.transform;
        GameObject sand_slant = Resources.Load("Land/Sand/Land_Sand_Slant") as GameObject;
        LandSlant[1] = sand_slant.transform;
        GameObject stone_slant = Resources.Load("Land/Stone/Land_Stone_Slant") as GameObject;
        LandSlant[2] = stone_slant.transform;
        GameObject water_slant = Resources.Load("Land/Water/Land_Water_Slant") as GameObject;
        LandSlant[3] = water_slant.transform;
        GameObject grass_Trio = Resources.Load("Land/Grass/Land_Grass_Corner_Trio") as GameObject;
        LandSlantCornerTrio[0] = grass_Trio.transform;
        GameObject sand_Trio = Resources.Load("Land/Sand/Land_Sand_Corner_Trio") as GameObject;
        LandSlantCornerTrio[1] = sand_Trio.transform;
        GameObject stone_Trio = Resources.Load("Land/Stone/Land_Stone_Corner_Trio") as GameObject;
        LandSlantCornerTrio[2] = stone_Trio.transform;
        GameObject water_Trio = Resources.Load("Land/Water/Land_Water_Corner_Trio") as GameObject;
        LandSlantCornerTrio[3] = water_Trio.transform;
        GameObject grass_Duo = Resources.Load("Land/Grass/Land_Grass_Corner_Duo") as GameObject;
        LandSlantCornerDuo[0] = grass_Duo.transform;
        GameObject sand_Duo = Resources.Load("Land/Sand/Land_Sand_Corner_Duo") as GameObject;
        LandSlantCornerDuo[1] = sand_Duo.transform;
        GameObject stone_Duo = Resources.Load("Land/Stone/Land_Stone_Corner_Duo") as GameObject;
        LandSlantCornerDuo[2] = stone_Duo.transform;
        GameObject water_Duo = Resources.Load("Land/Water/Land_Water_Corner_Duo") as GameObject;
		LandSlantCornerDuo[3] = water_Duo.transform;
        GameObject grass_peak = Resources.Load("Land/Grass/Land_Grass_Peak") as GameObject;
        LandPeak[0] = grass_peak.transform;
        GameObject sand_peak = Resources.Load("Land/Sand/Land_Sand_Peak") as GameObject;
        LandPeak[1] = sand_peak.transform;
        GameObject stone_peak = Resources.Load("Land/Stone/Land_Stone_Peak") as GameObject;
        LandPeak[2] = stone_peak.transform;
        GameObject water_peak = Resources.Load("Land/Water/Land_Water_Peak") as GameObject;
		LandPeak[3] = water_peak.transform;

        //setup edge type
        GameObject edge_invis = Resources.Load("Land/Edge/Land_Invisible") as GameObject;
        LandEdgeMask[0] = edge_invis.transform;
        GameObject edge = Resources.Load("Land/Edge/Land_Edge") as GameObject;
        LandEdgeMask[1] = edge.transform;

        //setup Path objects
        GameObject road_straight = Resources.Load("Path/Road_Straight") as GameObject;
        Path[0] = road_straight.transform;
        GameObject road_corner = Resources.Load("Path/Road_Corner") as GameObject;
        Path[1] = road_corner.transform;
        GameObject road_straight_slant_long = Resources.Load("Path/Road_Straight_Slant_Long") as GameObject;
        Path[2] = road_straight_slant_long.transform;
        GameObject road_straight_slant_short = Resources.Load("Path/Road_Straight_Slant_Short") as GameObject;
        Path[3] = road_straight_slant_short.transform;
        GameObject road_corner_slant_up_right = Resources.Load("Path/Road_Corner_Slant_Up_Right") as GameObject;
        Path[4] = road_corner_slant_up_right.transform;
        GameObject road_corner_slant_up_left = Resources.Load("Path/Road_Corner_Slant_Up_Left") as GameObject;
        Path[5] = road_corner_slant_up_left.transform;
        GameObject road_corner_slant_down_right = Resources.Load("Path/Road_Corner_Slant_Down_Right") as GameObject;
        Path[6] = road_corner_slant_down_right.transform;
        GameObject road_corner_slant_down_left = Resources.Load("Path/Road_Corner_Slant_Down_Left") as GameObject;
        Path[7] = road_corner_slant_down_left.transform;


        //setup objects
        GameObject castle = Resources.Load("Object/Object_Castle") as GameObject;
        Objects[0] = castle.transform;
        GameObject tower = Resources.Load("Object/Object_Tower_Round") as GameObject;
        Objects[1] = tower.transform;
        GameObject tower_wood = Resources.Load("Object/Object_Tower_Wood") as GameObject;
        Objects[2] = tower_wood.transform;

        //setup projectiles
        GameObject projectile_arrow = Resources.Load("Projectile/Projectile_Arrow") as GameObject;
        Projectile[0] = projectile_arrow.transform;
        GameObject projectile_stone = Resources.Load("Projectile/Projectile_Stone") as GameObject;
        Projectile[1] = projectile_stone.transform;
        
        //start map build
		if(skipStart == false)
		{
			worldCell home = new GameObject ("Tile " + start.ToString()).AddComponent<worldCell>().setup (start, 0, cellType.LandBasic, true, true, new int[1]{4});//this 0 forces the first tiles to be grass
            //attach(home.getTop(), new GameObject("Castle " + start.ToString()).AddComponent<worldCell>().setup(new Vector3(9, 9, 9), 0, cellType.Objects, true, false), Vector3.up);
		}
        else
        {
            worldCell home = get(start).GetComponent<worldCell>();
            Castle = home.getTop().neighbors[4];
        }

        UI_Singleton.getSingleton().begin();
        //Debug.Log(new Vector3(0, (float)0.5, 0));
		//home.neighbors[4] = new GameObject("Castle " + start.ToString()).AddComponent<worldCell>().setup(start + Vector3.up, 2, false, 1);
        //home.neighbors[4].neighbors[5] = home;    start + Vector3.up, 2, false, 1
	}

    private void buildCastle()// this has to be a sperate fuction because it has to run after the map is build, and the map builds in a different thread
    {
              
        worldCell startLand = get(start).GetComponent<worldCell>().getTop();        
        if (startLand.tileSet != cellType.LandBasic)
        {
            ExpandUp = false;
            startLand.tileSet = cellType.LandBasic;
            startLand.autoExpand = true;            
            lockFrameTimer = 5;
            locked = false;
            startLand.expand();// wait for expand to call bulid Castle again (hopefully with a basic land to place the tile on)
        }
        else
        {
            Castle = new GameObject("Object " + start.ToString()).AddComponent<worldCell>().setup(new Vector3(0, 0, 0), 0, cellType.Objects, true, false);
            attach(startLand, Castle, Vector3.up);
            Castle.gameObject.AddComponent<rail>().set(Vector3.zero);//this vector3.zero will not be used
            pathFind.findPath();
        }       
    }
	
	// Update is called once per frame
	void Update () {
		if (locked == false) {//mold land shapes while not locked
			lockFrameTimer++;
			Debug.Log ("lockFrameTimer = " + lockFrameTimer);
			if(lockFrameTimer > 10){
				locked = true;
                ExpandUp = true;
                if (Castle == null)
                {
                    buildCastle();
                }
			}
		}
	}

	//A good way to get a land spot by location.
	public static GameObject get(Vector3 location){
        GameObject temp = GameObject.Find("Tile " + location.ToString());
        if (temp != null)
        {
            return temp;
        }
        return null;
	}

	//turn direction in ints used from neighbors arrays. If only I could index arrays with Vector3s.
	public static int dirToInt(Vector3 dir){
		Vector3 direction = dir.normalized;
        //Debug.Log(dir +  " normalized to " + direction);
        switch (direction.ToString()) {
			case "(0.0, 0.0, 1.0)": return 0;//forward
            case "(-1.0, 0.0, 0.0)": return 1;//left
			case "(0.0, 0.0, -1.0)": return 2;//back
            case "(1.0, 0.0, 0.0)": return 3;//right
            case "(0.0, 1.0, 0.0)": return 4;//up
            case "(0.0, -1.0, 0.0)": return 5;//down
            
			case "(0.7, 0.0, 0.7)": return 6;//forward_right
            case "(0.7, 0.0, -0.7)": return 7;//back_right
            case "(-0.7, 0.0, 0.7)": return 8;//forward_left
            case "(-0.7, 0.0, -0.7)": return 9;//back_left

			case "(0.0, 0.7, 0.7)": return 10;//forward_up
			case "(0.0, 0.7, -0.7)": return 11;//back_up
			case "(0.0, -0.7, 0.7)": return 12;//forward_down
			case "(0.0, -0.7, -0.7)": return 13;//back_down
			case "(0.7, 0.7, 0.0)": return 14;//right_up
			case "(-0.7, 0.7, 0.0)": return 15;//left_up
			case "(0.7, -0.7, 0.0)": return 16;//right_down
			case "(-0.7, -0.7, 0.0)": return 17;//left_down

			default: return -1;//should not happen, but you never know.
		}
	}

    public static Vector3 intToDir(int direction){
        switch (direction)
        {
            case 0: return Vector3.forward;
            case 1: return Vector3.left;
            case 2: return Vector3.back;
            case 3: return Vector3.right;
            case 4: return Vector3.up;
            case 5: return Vector3.down;
            default: return Vector3.zero;
        }
    }

    //attach CellA to CellB in the direction from CellA to CellB
    public static bool attach(worldCell CellA, worldCell CellB, Vector3 direction){
        if(CellA != null && CellB != null && direction != null){
            int dir = dirToInt(direction);
            int revDir = dirToInt(direction * -1);
            if(dir >= 0 && dir <=5){//valid attach directions;
                CellA.neighbors[dir] = CellB;
                CellB.neighbors[revDir] = CellA;
                if (direction == Vector3.up && CellB.tileSet != map.cellType.Path)//dont want to move path tiles
                {
                    CellB.location = CellA.location + direction;
                }                
                return true;
            }
        }
        return false;
    }

    public static void setLayer(Transform trans, int layer)
    {
        //cmp.layer = layer;
        trans.gameObject.layer = layer;
        for (int I = 0; I < trans.childCount; I++)
        {
            setLayer(trans.GetChild(I), layer);
        }
    }

	public static void unlock(){
		lockFrameTimer = 0;
		locked = false;
	}

    public static bool ExpandUp
    {
        get
        {
            return expandUp;
        }
        set
        {
            expandUp = value;
        }
    }

    public static void reset()
    {
        pathFind.findPath();
    }
}