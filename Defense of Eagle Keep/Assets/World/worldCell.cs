﻿using UnityEngine;
//using System;
using System.Collections;
using System.Collections.Generic;

public class worldCell : MonoBehaviour, System.IEquatable<worldCell>, System.IComparable<worldCell>
{

    //working//public worldCell[] neighbors = new worldCell[4];
    public worldCell[] neighbors = new worldCell[6];//0 = forward, 1 = left, 2 = back, 3 = right, 4 = up, 5 = down
    public bool[] blockedDirection = new bool[6];
    List<int> neighborTypes = new List<int>();
    public Vector3 location;
    public Vector3 orientation;
    Transform cube = null;
    LayerMask mask;

    //public int oldTile;
    public int tile;
    public map.cellType tileSet;
    private bool forced;//allows the creater to force a land type if true;

    public bool visited = false;
    public int entryCost = int.MaxValue;
    public int costPathToHere = int.MaxValue;//BIG number

    public bool autoExpand; 
	//int tileType;

    public bool skipStart;

    // Use this for initialization
    void Start()
    {
        //gameObject.layer = 2;
        //BoxCollider bc = gameObject.AddComponent<BoxCollider>();
        //bc.size = new Vector3(1.1F, 1.1F, 1.1F);
        if (skipStart)//this is here so that I can use pre made maps without the whole thing trying to rebuild itself.
        {
            return;
        }
        if(!forced){// use this to pick the next land type as long as it has not been manualy selected already.
            tile = 0;//make sure that newTile does not stay -1;
            foreach (Vector3 dir in map.directions)
            {
                Vector3 modifiedLocation = (location + dir);
                GameObject go = map.get(modifiedLocation);
                if (go != null)
                {
                    worldCell script = go.GetComponent<worldCell>();
                    neighborTypes.Add(script.tile);
                }
            }
            //decide the winner of the surrounding land types that have already been selected
            int winner = Random.Range(0, neighborTypes.Count - 1);
            //use a random seed to decide the final land type selection bassed on % chances
            int seed = Random.Range(0, 100);
            if (seed < 80 && neighborTypes.Count > 0)
            {
                tile = neighborTypes[winner];
            }
            else if (seed < 87)
            {
                tile = 0;//grass
            }
            else if (seed < 90)
            {
                tile = 1;//sand
            }
            else if (seed < 95)
            {
                tile = 2;//stone
            }
            else
            {
                //I want water to be less prevelent at elevation
                if (location.y == 0 || Random.Range(0, 10) < 5 / location.y)
                {
                    tile = 3;//water
                }
                else
                {
                    tile = 0;
                }                
            }
        }
        entryCost = (tile * 4) + 1;
        link();  
        //if (autoExpand)
        //{
        expand();
        //}
        link();
        smooth();
        renderTile();
        skipStart = true;
    }

    // Update is called once per frame
    void Update()
    {
		if (map.locked == false && tileSet != map.cellType.Objects) {
				link ();
				smooth ();
				renderTile ();
		}
        if (Mask != 0)
        {
            map.setLayer(transform, Mask);
        }
        
    }

    public void renderTile()
    {
		if (cube != null) {
			Destroy (cube.gameObject);
		}
		//make sure this is not an edge
        //Note: Even if this is an edge tile, it will still has a newTile type for use if it ever gets explored. 
        //neighbors can see the newTile type aswell.
        if (location.x == 0 || location.z == 0 || location.x == map.width || location.z == map.length)
        {//is this and edge tile?
            //force the tile to be an edge.            
            if (location.z == 0)
            {
                cube = (Transform)Instantiate(map.LandEdgeMask[0]);
            }
            else
            {
                cube = (Transform)Instantiate(map.LandEdgeMask[1]);
            }
            //entryCost = int.MaxValue; // not sure if i want the pathing to be able to use the edges
            //visited = true;
        }
        else
        {
            //build tile of newTile type
			switch (tileSet)
            {
                case map.cellType.LandBasic: cube = (Transform)Instantiate(map.LandBasic[tile]); break;
                case map.cellType.LandSlant: cube = (Transform)Instantiate(map.LandSlant[tile]); break;
                case map.cellType.LandSlantCornerTrio: cube = (Transform)Instantiate(map.LandSlantCornerTrio[tile]); break;
                case map.cellType.LandSlantCornerDuo: cube = (Transform)Instantiate(map.LandSlantCornerDuo[tile]); break;
                case map.cellType.LandPeak: cube = (Transform)Instantiate(map.LandPeak[tile]); break;
                case map.cellType.LandEdgeMask: cube = (Transform)Instantiate(map.LandBasic[tile]); break;
                case map.cellType.Objects: cube = (Transform)Instantiate(map.Objects[tile]); break;
                case map.cellType.Path: cube = (Transform)Instantiate(map.Path[tile]); break;
            }
        }
        //make tile a child of this GameObject
        if (cube != null) {
			cube.transform.parent = transform;
			cube.transform.rotation = transform.rotation;
			cube.localPosition = Vector3.zero;
			transform.position = location;
            transform.LookAt (transform.position + orientation);
            //if (tileSet == 5 && neighbors[5].tileSet == 1)
            //{
            //    transform.Translate(Vector3.down);
            //}
			//Debug.Log("Child added to " + this.name);
		} else {
			Debug.Log("cube was null");
		}
        if (tileSet == map.cellType.LandSlantCornerDuo || tileSet == map.cellType.LandSlantCornerTrio || tileSet == map.cellType.LandPeak)
        {
            //cant path though these tile types
            entryCost = int.MaxValue;
        }
        else
        {
            //pathing though these tile types is valid
            entryCost = (tile*4) + 1;
        }
        cube.gameObject.layer = 0;
    }

    // setup a spicific type of worldCell. If loc vector is null, then this is a mobile type and location will have to be set elsewhere
    public worldCell setup(Vector3 loc, Vector3 ori, int newTile, map.cellType newTileSet, bool force, bool expand, int[] block)
    {
        location = loc;
        tile = newTile;
        forced = force;
        tileSet = newTileSet;
        autoExpand = expand;
        orientation = ori;
        for (int I = 0; I < neighbors.Length; I++)
        {
            blockedDirection[I] = false;
        }
        if (block != null)
        {
            foreach (int I in block)
            {
                blockedDirection[I] = true;
            }
        }
        return this;
    }

    public worldCell setup(Vector3 loc, int newTile, map.cellType newTileSet, bool force, bool expand, int[] block)
    {
        return setup(loc, Vector3.forward, newTile, newTileSet, force, expand, block);
    }

    // Just because it looks strange to me to send null to a funtion.
    public worldCell setup(Vector3 loc, int newTile, map.cellType newTileSet, bool force, bool expand)
    {
        return setup(loc, newTile, newTileSet, force, expand, null);
    }

    // setup that leaves the type open and lets you block and pick the tile set.
    public worldCell setup(Vector3 loc, Vector3 ori, map.cellType newTileSet, int[] block)
    {
        return setup(loc, ori, 0, newTileSet, false, true, block);
    }

    // setup that leaves the type open and lets you block.
    public worldCell setup(Vector3 loc, bool expand)
    {
        return setup(loc, 0, 0, false, expand, null);
    }

    // setup that leaves the type open and lets you block.
    public worldCell setup(Vector3 loc, int[] block)
    {
        return setup(loc, 0, 0, false, true, block);
    }

    // setup that leaves the type open.
    public worldCell setup(Vector3 loc)
    {
        return setup(loc, 0, 0, false, true, null);
    }

    // Expand the explored land to map bounds
    // expand() will also link all touching tiles to this one.
    public void expand()
    {
        List<Vector3> random = new List<Vector3>();
        if (autoExpand)
        {
            foreach (Vector3 dir in map.directions)
            {
                int temp = Random.Range(0, 2);
                //Debug.Log(""+temp);
                switch (temp)
                {
                    case 0: random.Insert(0, dir); break;
                    case 1: random.Add(dir); break;
                    default: break;
                }
            }
            //special non-recored directions
            random.Add(new Vector3(1, 0, 1));
            random.Add(new Vector3(1, 0, -1));
            random.Add(new Vector3(-1, 0, 1));
            random.Add(new Vector3(-1, 0, -1));
        }
        else
        {
            random.Add(Vector3.down);
        }

        //Create and link neighboring tiles.
        foreach (Vector3 dir in random)
        {
            Vector3 modifiedLocation = (location + dir);
            if (modifiedLocation.x <= map.width && modifiedLocation.x >= 0 && modifiedLocation.z <= map.length && modifiedLocation.z >= 0)
            {
                GameObject neighbor = map.get(modifiedLocation);
                if (neighbor == null)
                {
					if (location.y == 0 || Random.Range(0, 100) < 100 / (15 * location.y)|| dir == Vector3.down)
                    {
                        if(dir == Vector3.up && map.ExpandUp == true){
							//small chance of up ward expantion
							//if (Random.Range(0, 1000) > 980)
                            if (Random.Range(0, 1000) < (location.y) * 500 || (location.y == 0 && Random.Range(0,1000) < 70))
							{
								new GameObject("Tile " + modifiedLocation.ToString()).AddComponent<worldCell>().setup(modifiedLocation, tile, 0, true, true, null);
							}
						}
						else if(dir == Vector3.down){
							//always expand down if not bottom of the map
							if(modifiedLocation.y >= 0){
								new GameObject("Tile " + modifiedLocation.ToString()).AddComponent<worldCell>().setup(modifiedLocation, tile, 0, true, true, null);
							}
						}
						else{
							//make expanding tile
							new GameObject("Tile " + modifiedLocation.ToString()).AddComponent<worldCell>().setup(modifiedLocation);
						}                                     
                    }
                    else
                    {
                        if(dir != Vector3.up){
							//make a tile with no expantion
							new GameObject("Tile " + modifiedLocation.ToString()).AddComponent<worldCell>().setup(modifiedLocation, tile, 0, false, false, null);
						}                      
                    }
                }
            }
        }
    }

    public bool link()
    {
        if (tileSet == map.cellType.Path || tileSet == map.cellType.Objects)
        {
            return false;
        }
        GameObject neighbor;
        bool newContacts = false;
        foreach (Vector3 dir in map.directions)
        {
            if (neighbors[map.dirToInt(dir)] == null)
            {
				neighbor = map.get(location + dir);
				if (neighbor != null)
                {
					neighbors[map.dirToInt(dir)] = neighbor.GetComponent<worldCell>();
					if(dir == Vector3.down)
					{
						neighbors[map.dirToInt(dir)].tile = tile;
					}
					else{
						newContacts = true;
					}
				}
            }
        }
		return newContacts;
    }

    public void smooth()
    {
		//dont smoth objects
        if (tileSet < 0 || tileSet == map.cellType.Objects || tileSet == map.cellType.Path || location.y == 0 /*|| location.x == 0 || location.x == map.width || location.y == 0 || location.z == map.length*/)
        {
            return;
        }
        else if (neighbors[4] != null && neighbors[4].tileSet != map.cellType.LandSlantCornerDuo && neighbors[4].tileSet != map.cellType.Path)
        {
            //tileSet = 0;           
            //return;
            autoExpand = true;
            expand();
        }
        //link();//
        string contacts = "";
        GameObject neighbor;
        map.cellType[] tileSets = new map.cellType[4];
        foreach (Vector3 dir in map.directions) {
			//trip out up and down directions.
			if (dir != Vector3.up && dir != Vector3.down) {
				int intDir = map.dirToInt (dir);
				if (neighbors [map.dirToInt (dir)] == null) {
					neighbor = map.get (location + dir);
					if (neighbor != null) {
						neighbors [intDir] = neighbor.GetComponent<worldCell> ();
						tileSets [intDir] = neighbors [intDir].tileSet;
						contacts = contacts + dir;
					}
				} else {
					tileSets [intDir] = neighbors [intDir].tileSet;
					contacts = contacts + dir;
				}
			}
		}
        //Debug.Log(name + contacts);
        switch (contacts)
        {
            case "(0.0, 0.0, 1.0)(-1.0, 0.0, 0.0)(0.0, 0.0, -1.0)(1.0, 0.0, 0.0)"://contacts in each direction. 
                tileSet = 0;
				//Debug.Log ("(" + tileSets[0] + "," + tileSets[1] + "," + tileSets[2] + "," + tileSets[3] + ")");
                if (((tileSets[0] == map.cellType.LandSlant && map.dirToInt(neighbors[0].orientation) != 0) || tileSets[0] == map.cellType.LandSlantCornerDuo) && ((tileSets[1] == map.cellType.LandSlant && map.dirToInt(neighbors[1].orientation) != 1) || tileSets[1] == map.cellType.LandSlantCornerDuo))
	                {
                        tileSet = map.cellType.LandSlantCornerTrio;
	                    orientation = new Vector3(0, 0, -1);
	                }
                else if (((tileSets[1] == map.cellType.LandSlant && map.dirToInt(neighbors[1].orientation) != 1) || tileSets[1] == map.cellType.LandSlantCornerDuo) && ((tileSets[2] == map.cellType.LandSlant && map.dirToInt(neighbors[2].orientation) != 2) || tileSets[2] == map.cellType.LandSlantCornerDuo))
	                {
                        tileSet = map.cellType.LandSlantCornerTrio;
	                    orientation = new Vector3(1, 0, 0);
	                }
                else if (((tileSets[2] == map.cellType.LandSlant && map.dirToInt(neighbors[2].orientation) != 2) || tileSets[2] == map.cellType.LandSlantCornerDuo) && ((tileSets[3] == map.cellType.LandSlant && map.dirToInt(neighbors[3].orientation) != 3) || tileSets[3] == map.cellType.LandSlantCornerDuo))
                	{
                        tileSet = map.cellType.LandSlantCornerTrio;
                	    orientation = new Vector3(0, 0, 1);
                	}
                else if (((tileSets[3] == map.cellType.LandSlant && map.dirToInt(neighbors[3].orientation) != 3) || tileSets[3] == map.cellType.LandSlantCornerDuo) && ((tileSets[0] == map.cellType.LandSlant && map.dirToInt(neighbors[0].orientation) != 0) || tileSets[0] == map.cellType.LandSlantCornerDuo))
	                {
                        tileSet = map.cellType.LandSlantCornerTrio;
                        orientation = new Vector3(-1, 0, 0);
	                }                
                break;
            case "(-1.0, 0.0, 0.0)(0.0, 0.0, -1.0)(1.0, 0.0, 0.0)": tileSet = map.cellType.LandSlant; orientation = new Vector3(0, 0, 1); break;// contacts in 3 directions
            case "(0.0, 0.0, 1.0)(0.0, 0.0, -1.0)(1.0, 0.0, 0.0)": tileSet = map.cellType.LandSlant; orientation = new Vector3(-1, 0, 0); break; //
            case "(0.0, 0.0, 1.0)(-1.0, 0.0, 0.0)(1.0, 0.0, 0.0)": tileSet = map.cellType.LandSlant; orientation = new Vector3(0, 0, -1); break; //
            case "(0.0, 0.0, 1.0)(-1.0, 0.0, 0.0)(0.0, 0.0, -1.0)": tileSet = map.cellType.LandSlant; orientation = new Vector3(1, 0, 0); break; //
            case "(0.0, 0.0, 1.0)(-1.0, 0.0, 0.0)": tileSet = map.cellType.LandSlantCornerDuo; orientation = new Vector3(0, 0, 1); /*entryCost = int.MaxValue*/; break;     // contacts in 2 directions
            case "(-1.0, 0.0, 0.0)(0.0, 0.0, -1.0)": tileSet = map.cellType.LandSlantCornerDuo; orientation = new Vector3(-1, 0, 0); /*entryCost = int.MaxValue*/; break;   //
            case "(0.0, 0.0, -1.0)(1.0, 0.0, 0.0)": tileSet = map.cellType.LandSlantCornerDuo; orientation = new Vector3(0, 0, -1); /*entryCost = int.MaxValue*/; break;    //
            case "(0.0, 0.0, 1.0)(1.0, 0.0, 0.0)": tileSet = map.cellType.LandSlantCornerDuo; orientation = new Vector3(1, 0, 0); /*entryCost = int.MaxValue*/; break;      //
            case "(0.0, 0.0, 1.0)(0.0, 0.0, -1.0)": tileSet = map.cellType.LandBasic; break;
            case "(-1.0, 0.0, 0.0)(1.0, 0.0, 0.0)": tileSet = map.cellType.LandBasic; break;
            case "(0.0, 0.0, 1.0)": tileSet = map.cellType.LandBasic; orientation = new Vector3(0, 0, 1); break;    // contacts in 1 direction
            case "(-1.0, 0.0, 0.0)": tileSet = map.cellType.LandBasic; orientation = new Vector3(-1, 0, 0); break;  //
            case "(0.0, 0.0, -1.0)": tileSet = map.cellType.LandBasic; orientation = new Vector3(0, 0, -1); break;  //
            case "(1.0, 0.0, 0.0)": tileSet = map.cellType.LandBasic; orientation = new Vector3(1, 0, 0); break;    //
            case "": tileSet = map.cellType.LandBasic; break;
            //case "(0.0, 0.0, 1.0)(-1.0, 0.0, 0.0)(0.0, 0.0, -1.0)(1.0, 0.0, 0.0)": tileSet = 0; break;
            default: this.tileSet = map.cellType.LandBasic; break;
        }
        //speacial cases
        if (neighbors[4] != null && tileSet == map.cellType.LandSlant && neighbors[4].tileSet == map.cellType.LandSlantCornerDuo)//under hill
        {
            tileSet = map.cellType.LandSlantCornerTrio;
            entryCost = int.MaxValue;
            orientation = neighbors[4].orientation;
        }
        if  (//opposing slant right
            tileSet == map.cellType.LandSlant &&
            neighbors[map.dirToInt(Vector3.Cross(Vector3.up, orientation))].tileSet == map.cellType.LandSlant &&
            neighbors[map.dirToInt(Vector3.Cross(Vector3.up, orientation))].orientation != orientation
            )
        {
            autoExpand = true;
            expand();
        }
        if  (//opposing slant left
            tileSet == map.cellType.LandSlant &&
            neighbors[map.dirToInt(Vector3.Cross(Vector3.up, orientation) * -1)].tileSet == map.cellType.LandSlant &&
            neighbors[map.dirToInt(Vector3.Cross(Vector3.up, orientation)*-1)].orientation != orientation 
            )
        {
            autoExpand = true;
            expand();
        }
        else if (tileSets[0] == map.cellType.LandSlant && tileSets[1] == map.cellType.LandSlant && tileSets[2] == map.cellType.LandSlant && tileSets[3] == map.cellType.LandSlant && tileSet == map.cellType.LandSlantCornerTrio)
        {//Peak
            tileSet = map.cellType.LandPeak;
            if (neighbors[0].orientation == Vector3.right)
            {
                orientation = Vector3.right;
            }
            else
            {
                orientation = Vector3.forward;
            }
        }
    }

    //find the top land cell the stack containing this cell
    public worldCell getTop()
    {
        worldCell top = this;
        while (top.neighbors[4] != null && top.neighbors[4].tileSet != map.cellType.Objects && top.neighbors[4].tileSet != map.cellType.Path)//do not find objects or paths
        {
            top = top.neighbors[4];
        }
        return top;
    }

    public Transform Cube
    {
        get
        {
            return cube;
        }
    }

    public LayerMask Mask
    {
        get
        {
            return mask;
        }
        set
        {
            mask = value;
        }
    }
    
    void OnMouseEnter()
    {
        transform.position = location + new Vector3(0, .2F, 0);
    }

    void OnMouseExit()
    {
        transform.position = location;
    }
    
    public int CompareTo(worldCell cell)
    {
        if (cell == null)
        {
            return 1;
        }
        return costPathToHere.CompareTo(cell.costPathToHere);
    }

    public bool Equals(worldCell cell)
    {
        if (cell == null) return false;
        return (this.location.Equals(cell.location));
    }
}


