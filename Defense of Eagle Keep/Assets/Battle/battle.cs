﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//106 == best score.
public class battle : MonoBehaviour {

    private static battle theBattle;

    private List<enemy> targets;
    private List<worldCell> towers;
    private float keepMaxHealth;
    public float keepCurrentHealth;//this is public so i can see in the editor.
    private float timer;
    private float spawnTime;
    private float spawnCount;
    private int wave;
    private bool gameActive;

    // Use this for initialization
    private battle()
    {
        targets = new List<enemy>();
        towers = new List<worldCell>();
        //reset();
    }
    
    void Start () {
        theBattle = this;
        reset();
	}
	
	// Update is called once per frame
	void Update () {
        if (gameActive)
        {
            if (timer >= spawnTime)//spawn more and more units each wave. Easy way to make the game harder.
            {
                timer = 0;
                if (wave == 15)
                {
                    gameActive = false;
                    UI_Singleton.getSingleton().gameVictory();
                }
                else
                {
                    wave++;
                    spawnCount += (spawnCount / 4);
                    StartCoroutine("spawn");
                    UI_Singleton.getSingleton().displayWave(wave);
                }
            }
            timer += Time.deltaTime;
            targets.Sort();
        }
	}

    protected IEnumerator spawn()
    {
        for (int I = 0; I < spawnCount; I++)
        {
            GameObject unit = Instantiate(Resources.Load("Unit/Unit_Temp")) as GameObject;
            unit.transform.name = "Unit " + Random.Range(0, 100000);
            unit.transform.GetComponent<enemy>().setup(100, 5, 10);
            yield return new WaitForSeconds(5 / spawnCount);
        }
    }

    public static battle getBattleSingleton()
    {
        if (theBattle == null)
        {
            theBattle = new battle();
        }
        return theBattle;
    }

    public void registerEnemy(enemy target){
        targets.Add(target);
        //Debug.Log(target + "added to targets list");
    }

    public bool removeEnemy(enemy target)
    {
        //Debug.Log(target + "is being removed from targets list");
        return targets.Remove(target);
    }

    public void registerTower(worldCell tower)
    {
        towers.Add(tower);
        //Debug.Log(target + "added to targets list");
    }

    public bool removeTower(worldCell tower)
    {
        //Debug.Log(target + "is being removed from targets list");
        return towers.Remove(tower);
    }

    public float hitKeep(float damage){
        keepCurrentHealth -= damage;
        Debug.Log("Keep hit! Health: " + keepCurrentHealth + "/" + keepMaxHealth + ".");
        UI_Singleton.getSingleton().displayHealth(keepCurrentHealth, keepMaxHealth);
        if (keepCurrentHealth <= 0 && gameActive)
        {
            gameActive = false;
            UI_Singleton.getSingleton().gameOver();
        }
        return keepCurrentHealth;
    }

    public List<enemy> EnemyList
    {
        get
        {
            return targets;
        }        
    }

    public List<worldCell> TowerList
    {
        get
        {
            return towers;
        }
    }

    public void reset()
    {
        keepMaxHealth = 100;
        keepCurrentHealth = 100;
        spawnTime = 20;
        timer = 10;
        spawnCount = 3;
        wave = 0;
        gameActive = true;

        StopAllCoroutines();

        worldCell temp;
        for (int I = towers.Count - 1; I >= 0; I--)
        {
            temp = towers[I];
            removeTower(temp);
            temp.neighbors[5].neighbors[4] = null;
            Destroy(temp.transform.gameObject);
        }

        enemy tempNME;
        for (int I = targets.Count - 1; I >= 0; I--)
        {
            tempNME = targets[I];
            removeEnemy(tempNME);
            Destroy(tempNME.transform.gameObject);
        }

        if (UI_Singleton.getSingleton() != null)
        {
            UI_Singleton.getSingleton().displayHealth(keepCurrentHealth, keepMaxHealth);
        }        
    }
}
