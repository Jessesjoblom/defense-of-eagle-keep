﻿using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour, System.IEquatable<enemy>, System.IComparable<enemy>
{

    protected float currentHealth;
    protected float maxHealth;
    protected float speed;
    protected float attackDamage;

    private bool ready = false;

    //public unitControl controler;
    
    public void setup(float setHealth, float setSpeed, float setAttackDamage)
    {
        maxHealth = setHealth;
        currentHealth = maxHealth;
        speed = setSpeed;
        attackDamage = setAttackDamage;
        battle.getBattleSingleton().registerEnemy(this);
        transform.gameObject.AddComponent<unitControl>().setup(speed);
        ready = true;
    }

    // Use this for initialization
	protected void Start () {
	}
	
	// Update is called once per frame
    protected void Update()
    {
        //if (!ready)//dont want to start updateing until setup is done. Simulate a constructor
        //{
        //    return;
        //}
        //if (currentHealth <= 0)
        //{
        //    if (battle.getBattleSingleton().removeEnemy(this) == true)
        //    {
        //        Destroy(transform.gameObject);
        //    }            
        //}
	}

    public void hit(float damage)
    {
        currentHealth -= damage;
        Debug.Log("Unit: " + this + " heath: " + currentHealth + "/" + maxHealth + ".");
        if (currentHealth <= 0)
        {
            battle.getBattleSingleton().removeEnemy(this);
            economyControl.getSingleton().deposit(1);
            Destroy(transform.gameObject);           
        }
    }

    public int CompareTo(enemy brotherInArms)
    {
        if (brotherInArms == null)
        {
            return 1;
        }
        if (this != null)
        {
            float ourDist = Vector3.Distance(pathFind.pathStart.transform.position, transform.position);
            float brotherInArmsDist = Vector3.Distance(pathFind.pathStart.transform.position, brotherInArms.transform.position);
            return ourDist.CompareTo(brotherInArmsDist);
        }
        else
        {
            return 1;
        }
        
    }

    public bool Equals(enemy brotherInArms)
    {
        //Debug.Log("Equals? " + transform.name + " : " + brotherInArms.transform.name);
        if (transform.name == brotherInArms.transform.name)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public float AttackDamage
    {
        get
        {
            return attackDamage;
        }
    }
}
