﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class projectileStone : projectileControl {

	// Use this for initialization
	new void Start () {
        base.Start();
        damage = 20;
        speed = 3;
	}

    new void Update()
    {
        base.Update();
    }

    public override void applyDamage()//blockable AOE(will not traval though a taget to hit others)
    {
        //Debug.Log("Child applyDamage Called");
        List<enemy> inAOERange = new List<enemy>();
        foreach (enemy nme in battle.getBattleSingleton().EnemyList)
        {
            if (nme != null && destination != null && Vector3.Magnitude(nme.transform.position - destination) <= 1)
            {
                inAOERange.Add(nme);
            }
        }
        enemy[] nmes = inAOERange.ToArray();
        for (int I = 0; I < nmes.Length; I++)
        {
            Debug.Log("splash damage done to: " + nmes[I]);
            nmes[I].hit(damage);
        }       
        Destroy(transform.gameObject);
    }
}
