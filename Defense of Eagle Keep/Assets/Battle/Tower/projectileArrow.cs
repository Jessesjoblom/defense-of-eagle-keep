﻿using UnityEngine;
using System.Collections;

public class projectileArrow : projectileControl {

	// Use this for initialization
	new void Start () {
        base.Start();
        damage = 20;
        speed = 7;
	}

    new void Update()
    {
        if (target != null)
        {
            destination = target.position;
        }        
        base.Update();
    }

    public override void applyDamage(){
        if (target != null)
        {
            target.GetComponent<enemy>().hit(damage);
        }        
        Destroy(transform.gameObject);
    }
}
