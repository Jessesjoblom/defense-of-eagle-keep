﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class towerWeapon : MonoBehaviour {

    List<enemy> targets;
    Vector3 location;
    public float range;
    public float reloadRate;
    public float reloadProgress;
    public int ProjectileType;
    //public float inRange;
    //public float inDamage;

    public void setup(float setRange, float setReloadRate)
    {
        range = setRange + transform.position.y;
        reloadRate = setReloadRate;
    }

    // Use this for initialization
	void Start () {
        location = transform.position;
        //setup(6, 1);
        reloadProgress = 0;
        range += transform.position.y - .5F;
	}
	
	// Update is called once per frame
	void Update () {
        targets = battle.getBattleSingleton().EnemyList;
        Ray ray;
        Vector3 targetLocation;
        //Vector3 trajectory;
        if (reloadProgress <= 1)
        {
            reloadProgress += reloadRate * Time.deltaTime;
        }
        foreach (enemy target in targets)
        {
            targetLocation = target.transform.position;
            ray = new Ray(location, targetLocation - location);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, range, ~(1<<8)))//this should ignore layer 8 which is the tower transforms(tower can shoot through each other).
            {
                if (hit.transform.Equals(target.transform))
                {
                    //Debug.Log("Target found");
                    if (reloadProgress >= 1)
                    {
                        Instantiate(map.Projectile[ProjectileType]).GetComponent<projectileControl>().setup(transform, target.transform);
                        reloadProgress = 0;
                    }
                    return;
                }
            }
        }
	}
}
