﻿using UnityEngine;
using System.Collections;

public abstract class projectileControl : MonoBehaviour {

    //protected Transform origin;
    protected Transform target;
    protected Vector3 origin;
    protected Vector3 destination;
    protected Vector3 trajectory;
    protected float damage;
    protected float speed;
    protected float progress;
    // Use this for initialization
    protected void Start()
    {
        //damage = 50;
        //speed = 20;
        progress = 0;
	}
	
	// Update is called once per frame
    protected void Update()
    {
        if (progress >= 1 && destination != null)
        {
            applyDamage();
        }
        else
        {
            progress += Time.deltaTime * speed;
        }
        if (destination != null)
        {
            trajectory = destination - origin;
            transform.position = Vector3.Lerp(origin, destination, progress);
            transform.LookAt(destination);
        }
        else
        {
            Destroy(transform.gameObject);
        }
        
	}

    public void setup(Transform setOrgin, Transform setDestination, float inDamage, float inSpeed){
        damage = inDamage;
        speed = inSpeed;
        setup(setOrgin, setDestination);
    }

    public void setup(Transform setOrgin, Transform setDestination)
    {
        origin = setOrgin.position;
        destination = setDestination.position;
        target = setDestination;
    }

    public abstract void applyDamage();
    //{
        //if (destination != null)
        //{
        //    target.GetComponent<enemy>().hit(damage);
        //}        
        //Destroy(transform.gameObject);
    //}
}
