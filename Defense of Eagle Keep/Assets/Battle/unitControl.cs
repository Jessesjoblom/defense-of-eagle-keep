﻿using UnityEngine;
using System.Collections;

public class unitControl : MonoBehaviour {
    protected rail pathRail;
    protected Vector3 from;
    protected Vector3 to;
    protected float speed;
    protected float t;
	// Use this for initialization

    protected bool ready = false;

    public void setup(float s)
    {
        speed = s;
        pathRail = pathFind.pathEnd.transform.GetComponent<rail>();
        //pathRail.getNext(this);
        pathRail.accept(this);
        transform.position = to;
        //speed = 5;
        ready = true;
    }

    protected void Start () {
	}
	
	// Update is called once per frame
    protected void Update()
    {
        if (!ready)
        {
            return;
        }
        if (transform.position == pathFind.pathStart.location)//should be when the center of the castle is reached.
        {
            Debug.Log("Unit " + this + " has reached the castle and been removed.");
            enemy thisEnemy = transform.GetComponent<enemy>();
            battle.getBattleSingleton().removeEnemy(thisEnemy);
            battle.getBattleSingleton().hitKeep(thisEnemy.AttackDamage);
            Destroy(this.transform.gameObject);                       
            return;
        }
        else if (transform.position == Rail.Center)
        {
            to = Rail.Exit;
            from = transform.position;
            t = 0;
            //while (pathRail.getNext(this) == false) { };
            //t = 0;
        }
        else if (transform.position == Rail.Exit)
        {
            //while (Rail.NextRail == null) { } //spin till ready;
            Rail.NextRail.accept(this);
        }
		t += speed * Time.deltaTime;
		this.transform.position = Vector3.Lerp (from, to, t);
	}

    //modification of the visitor pattern. 
    public void visit(rail inRail)
    {
        Rail = inRail;
        to = Rail.Center;
        from = transform.position;
        t = 0;
    }

    public rail Rail
    {
        get
        {
            return pathRail;
        }
        set
        {
            pathRail = value;
        }
    }

    public Vector3 From
    {
        get
        {
            return from;
        }
        set
        {
            from = value;
        }
    }

    public Vector3 To
    {
        get
        {
            return to;
        }
        set
        {
            to = value;
        }
    }
}
