﻿using UnityEngine;
using System.Collections;

public class economyControl : MonoBehaviour {

    private static economyControl singleton;
    public int money;// so that I can edit it for testing
	
    // Use this for initialization
	void Start () {
        singleton = this;
        money = 20;
        //money = 1000;
        //deposit(20);
        //while (UI_Singleton.getSingleton() == null) { }; //spin till UI is ready
        //UI_Singleton.getSingleton().displayMoney(money);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool deposit(int newMoney)
    {
        if (newMoney > 0)
        {
            money += newMoney;
            UI_Singleton.getSingleton().displayMoney(money);
            return true;
        }
        return false;
    }

    public bool withdraw(int lessMoney)
    {
        if (isWithdrawPossible(lessMoney))
        {
            money -= lessMoney;
            UI_Singleton.getSingleton().displayMoney(money);
            return true;
        }
        return false;
    }

    public bool isWithdrawPossible(int lessMoney)
    {
        if (lessMoney > 0 && money - lessMoney >= 0)
        {
            return true;
        }
        return false;
    }

    public int getBalance()
    {
        return money;
    }

    public static economyControl getSingleton()
    {
        if (singleton == null)
        {
            singleton = new economyControl();
        }
        return singleton;
    }

    public void reset()
    {
        money = 20;
        if (UI_Singleton.getSingleton() != null)
        {
            UI_Singleton.getSingleton().displayMoney(money);
        }
    }
}
