### Defense of Eagle Keep  ###

* Defense of Eagle Keep is a new Tower Defense game focused on a procedurally generated landscape and recourse management. *
* Defense of Eagle Keep is currently a pre-alpha *

### Instructions ###

1. Download Defense of Eagle from the downloads section and extract from the zip folder.
2. Run in windowed mode or full screen native resolution. (resolution miss-match will mess up the mouse accuracy in game)
3. Arrow keys move the camera.
4. Towers have to be selected from the bottom bar before they can be placed. (Selected tower will be the last one you clicked, not nice graphic for telling the selected tower at this time)
5. M icon in the top left gives access the menu.

### Who do I talk to? ###

I can be contacted at jessesjoblom@outlook.com